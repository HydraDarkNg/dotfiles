(setq *dotfiles* (expand-file-name "~/.dotfiles/"))

(setq bookmark-file (concat *dotfiles* "bookmarks"))

(setq warning-minimum-level :emergency)

(setq gc-cons-threshold (* 100 1024 1024))

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)

(package-initialize)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(require 'use-package)

(dolist (string  '("hydrang-lisp" "hydrang-emacs-modules"))
  (add-to-list 'load-path (locate-user-emacs-file string)))

(recentf-mode t)

(require 'hydrang-elisp)
(require 'hydrang-keybindings)
(require 'hydrang-org)
(require 'hydrang-notes)
(require 'hydrang-ui)
(require 'hydrang-modeline)
(require 'hydrang-ia)
(require 'hydrang-study)
(require 'hydrang-books)
(require 'hydrang-term)
(require 'hydrang-games)

(use-package hyperdrive
  :straight t
  :pin "nongnu"
  :bind ("C-c h" . hyperdrive-menu)
  :init (hyperdrive-menu-bar-mode 1))

(defgroup hydrang-mathematics ()
  "Mis configuraciones especiales para tratar con notas matemáticas.")

(defcustom hydrang-mathematics-note-types
  '("definitions" "theorems" "demonstration" "notes")
  "Los tipos de notas que se usan en matemáticas."
  :type 'list
  :group 'hydrang-mathematics)

(defcustom hydrang-mathematics-notes-directory
  (concat denote-directory "/Mathematics/")
  "El directorio donde tengo mis notas de matemáticas."
  :type 'path
  :group 'hydrang-mathematics)

(defun hydrang-mathematics-type-prompt ()
  (completing-read "Type: " hydrang-mathematics-note-types))

(defun hydrang-mathematics (title type)
  (interactive (list (denote-title-prompt)
		     (hydrang-mathematics-type-prompt)))
  (denote title (list type) 'org hydrang-mathematics-notes-directory nil 'mathematics))

(push
 '(mathematics . "* Idea\n* Formally")
 denote-templates)

(use-package vertico
  :straight t
  :bind
  (:map vertico-map
	("C-j" . vertico-next)
	("C-k" . vertico-previous))
  :init
  (vertico-mode t))

(use-package consult
  :straight t
  :bind
  ([remap switch-to-buffer] . consult-buffer)
  ([remap isearch-forward] . consult-line))

(use-package vertico-directoy
  :after vertico
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word)))

(use-package marginalia
  :straight t
  :custom
  (marginalia-max-relative-age 0)
  (marginalia-align 'right)
  :init
  (marginalia-mode t))

(use-package orderless
  :straight t
  :init
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package corfu
  :straight t
  :bind
  (:map corfu-map
	(("C-j" . corfu-next)
	 ("C-k" . corfu-previous)))
  :init
  (global-corfu-mode t)
  (setq tab-always-indent 'complete))

(use-package corfu-popupinfo
  :custom
  (corfu-popupinfo-delay 0.25)
  :config
  (corfu-popupinfo-mode))

(use-package nerd-icons-corfu
  :config (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter)
  :straight (:host github :repo "LuigiPiucco/nerd-icons-corfu"))

(use-package nerd-icons-completion
  :straight t
  :hook (marginalia-mode . nerd-icons-completion-marginalia-setup)
  :init
  (nerd-icons-completion-mode))

(use-package highlight-indent-guides
  :straight t
  :hook (prog-mode . highlight-indent-guides-mode)
  :init
  (setq highlight-indent-guides-method 'character
        highlight-indent-guides-responsive 'top
        highlight-indent-guides-suppress-auto-error t))

(use-package helpful
  :straight t
  :bind
  ([remap describe-function] . helpful-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key)
  ([remap describe-symbol] . helpful-symbol))

(transient-define-prefix transient--helpful ()
  ["Help commands"
   [("f" "Describe function" helpful-function)
    ("c" "Describe command" helpful-command)
    ("v" "Describe variable" helpful-variable)]
   [("k" "Describe key" helpful-key)
    ("s" "Describe symbol" helpful-symbol)]])

(emacs-commands
  "h" 'transient--helpful)

(use-package tempel
  :straight t
  :custom
  (tempel-path (expand-file-name "~/.dotfiles/templates/templates"))
  :bind (("M-+" . tempel-complete)
         ("M-*" . tempel-inset)))

(use-package embark
  :straight t
  :bind
  ("C-." . embark-act)
  :config
  (keymap-set embark-symbol-map "h" #'helpful-symbol)
  (keymap-set embark-become-help-map "s" #'helpful-symbol))

(use-package embark-consult
  :straight t
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package which-key
  :straight t
  :init
  (which-key-mode t))

(use-package sly
  :straight t
  :custom
  (sly-symbol-completion-mode nil))

(transient-define-prefix transient--common-lisp ()
  ["Eval"
   ("d" "Function" sly-eval-defun)
   ("r" "Region" sly-eval-region)
   ("b" "Buffer" sly-eval-buffer)])

(evil-define-key '(visual normal) lisp-mode-map (kbd "SPC e") #'transient--common-lisp)

(use-package dired
  :custom
  (dired-listing-switches "-lahv --group-directories-first")
  :general
  (:states 'normal
   :keymaps 'dired-mode-map
   "SPC up" 'dired-up-directory
   "SPC k" 'kill-buffer
   "SPC f" 'find-file
   "SPC b" 'consult-buffer
   "SPC p" 'transient--project
   "SPC o" 'other-window))

(use-package dired-open
  :straight t
  :config
  (setq dired-open-extensions '(("jpg" . "feh")
                                ("png" . "feh")
                                ("xopp" . "flatpak run com.github.xournalpp.xournalpp")
				("ggb" . "flatpak run org.geogebra.GeoGebra")
				("docx" . "libreoffice24.2")
				("xlxs" . "libreoffice24.2"))))

(use-package dired-hide-dotfiles
  :straight t
  :config
  (defun my-dired-mode-hook ()
    "My `dired' mode hook."
    ;; To hide dot-files by default
    (dired-hide-dotfiles-mode))

  (define-key dired-mode-map "." #'dired-hide-dotfiles-mode)
  (add-hook 'dired-mode-hook #'my-dired-mode-hook))

(use-package nerd-icons-dired
  :straight t
  :hook (dired-mode . nerd-icons-dired-mode))

(use-package telega
  :straight t
  :defer t
  :commands (telega)
  :custom
  (telega-emoji-use-images nil)
  (telega-directory (expand-file-name "~/.cache/telega"))
  :config  
  (setq telega-server-libs-prefix "~/.compilations/td/tdlib/")
  (require 'ol-telega))

(use-package tex
  :straight auctex)

(use-package xenops
  :straight (:type git :host github :repo "dandavison/xenops"))

(with-eval-after-load 'tex
  (setf (cdr (assq 'output-pdf TeX-view-program-selection)) '("PDF Tools"))
  (setq TeX-source-correlate-method 'synctex)
  (TeX-source-correlate-mode)
  (setq TeX-source-correlate-start-server nil)
  (add-hook 'TeX-mode-hook 'prettify-symbols-mode))

(defun isearch-repair ()
  (interactive)
  (isearch-forward))

;; PDF view
(use-package pdf-tools
  :bind (:map pdf-view-mode-map
              ("C-s" . isearch-repair))
  :config
  (pdf-tools-install)
  (setq pdf-view-midnight-colors
        (cons
	 (ef-themes-get-color-value 'fg-main nil 'ef-autumn)
	 (ef-themes-get-color-value 'bg-main nil 'ef-autumn)))
  (add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode))

(use-package pdf-view-restore
  :straight t
  :after pdf-tools
  :custom
  (pdf-view-restore-filename "~/.config/emacs/.pdf-view-restore")
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode))

(use-package djvu
  :straight t)

(use-package org-noter
  :straight t)

(use-package org-pdftools
  :straight t
  :hook (org-mode . org-pdftools-setup-link))

(defun org-pdf-app (file-path link-without-schema)
  "Open pdf file using pdf-tools and go to the specified page."
  (org-pdftools-open-pdftools link-without-schema))

(push '("\\.pdf\\(::[0-9]+\\)?\\'" . org-pdf-app)
      org-file-apps)

(use-package emms
  :straight (:repo "https://git.savannah.gnu.org/git/emms.git")
  :custom
  (emms-browser-default-browse-type 'info-artist)
  :config
  ;; (require 'emms-info-tinytag)
  (require 'emms-setup)
  (require 'emms-player-simple)
  (require 'emms-source-file)
  (require 'emms-source-playlist)
  (require 'emms-streams)
  (require 'emms-player-mpv)
  (require 'emms-player-mpd)
  (emms-all)
  (setq emms-info-functions '())
  (setq emms-info-tinytag-python-name "python3")
  (setq emms-source-file-default-directory (expand-file-name "~/Música/"))
  (push 'emms-player-mpv emms-player-list)
  (setq-default
   emms-player-mpv-environment '("PULSE_PROP_media.role=music")
   emms-player-mpv-parameters '("--quiet" "--really-quiet" "--no-audio-display" "--force-window=no" "--vo=null" "--no-video" ""))
  (global-set-key (kbd "<f7>") 'emms-smart-browse))

(use-package ready-player
  :straight (:type git :host github :repo "xenodium/ready-player")
  :config
  (ready-player-add-to-auto-mode-alist))

(use-package magit
  :straight t)

(transient-define-prefix transient--project ()
  ["Projects" 
   [("!" "Shell Command" project-shell-command)
    ("&" "Async shell command" project-async-shell-command)
    ("Cb" "List buffers" project-list-buffers)
    ("D" "Dired" project-dired)
    ("F" "Project or external find file" project-or-external-find-file)
    ("G" "Project or external find regexp" project-or-external-find-regexp)
    ("b" "Switch to buffer" project-switch-to-buffer)]
   [("c" "Compile" project-compile)
    ("d" "Find directory" project-find-dir)
    ("e" "Eshell" project-eshell)
    ("f" "Find File" project-find-file)
    ("g" "Find Regexp" project-find-regexp)
    ("k" "Kill buffers" project-kill-buffers)
    ("o" "Any command" project-any-command)]
   [("p" "Switch project" project-switch-project)
    ("r" "Query replace regexp" project-query-replace-regexp)
    ("s" "Shell" project-shell)
    ("v" "Magit" magit)
    ("x" "Execute extended-command" project-execute-extended-command)]])

(emacs-commands
  "p" 'transient--project)

(defun eshell/clear ()      
   (let ((eshell-buffer-maximum-lines 0)) (eshell-truncate-buffer)))

(setq ispell-program-name "aspell"
      ispell-dictionary "castellano")

(defvar *dictionary* '("castellano" "english" "francais"))

(defun change-lang (lang)
  (interactive (list (completing-read "Idioma: " *dictionary*)))
  (setq ispell-dictionary lang)
  (when (yes-or-no-p "¿Desea iniciar flyspell? ")
    (flyspell-mode -1)
    (flyspell-mode 1)))

(setq auth-sources '("~/.authinfo.gpg"))
(setf epa-pinentry-mode 'loopback)

(defun lookup-password (&rest keys)
  (let ((result (apply #'auth-source-search keys)))
    (if result
        (funcall (plist-get (car result) :secret))
      nil)))

(defun lookup-account (&rest keys)
  (let ((result (apply #'auth-source-search keys)))
    (if result
        (plist-get (car result) :user)
      nil)))

(use-package mu4e
  :defer t
  :commands mu4e
  :config
  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir (expand-file-name "~/Documentos/Correo"))

  (setq mu4e-contexts
        (list
         ;; Work account
         (make-mu4e-context
          :name "Gmail"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/Gmail" (mu4e-message-field msg :maildir))))
          :vars `((user-mail-address . ,(lookup-account :host "gmail"))
                  (user-full-name    . "HydraDarkNg")
                  (mu4e-drafts-folder  . "/Gmail/[Gmail]/Drafts")
                  (mu4e-sent-folder  . "/Gmail/[Gmail]/Sent Mail")
                  (mu4e-refile-folder  . "/Gmail/[Gmail]/All Mail")
                  (mu4e-trash-folder  . "/Gmail/[Gmail]/Trash")))

         ;; Personal account
         (make-mu4e-context
          :name "Yahoo"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/Yahoo" (mu4e-message-field msg :maildir))))
          :vars `((user-mail-address . ,(lookup-account :host "yahoo"))
                  (user-full-name    . "HydraDarkNg")
                  (mu4e-drafts-folder  . "/Yahoo/Drafts")
                  (mu4e-sent-folder  . "/Yahoo/Sent")
                  (mu4e-refile-folder  . "/Yahoo/Archive")
                  (mu4e-trash-folder  . "/Yahoo/Trash")))))

  (setq mu4e-maildir-shortcuts
        '(("/Inbox"             . ?i)
	  ("/Gmail/Escuela"     . ?e)
          ("/Gmail/[Gmail]/Sent Mail" . ?s)
          ("/Gmail/[Gmail]/Trash"     . ?t)
          ("/Gmail/[Gmail]/Drafts"    . ?d)
          ("/Gmail/[Gmail]/All Mail"  . ?a))))

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program (expand-file-name "~/bin/LibreWolf.AppImage"))

(use-package elfeed
  :straight t
  :hook
  (elfeed-show-mode . olivetti-mode)
  :custom
  (elfeed-search-filter "@6-months-ago +unread -nodisplay")
  (elfeed-db-directory "~/.cache/elfeed"))

(use-package elfeed-org
  :straight t
  :custom
  (rmh-elfeed-org-files (list hydrang-organize-file))
  :init
  (elfeed-org))

(use-package elfeed-tube
  :straight t
  :after elfeed
  :demand t
  :custom
  (elfeed-tube-thumbnail-size 'large)
  (elfeed-tube-invidious-url "http://localhost:3000")
  :config
  ;; (setq elfeed-tube-auto-save-p nil) ; default value
  ;; (setq elfeed-tube-auto-fetch-p t)  ; default value
  (elfeed-tube-setup)

  :bind (:map elfeed-show-mode-map
         ("F" . elfeed-tube-fetch)
         ([remap save-buffer] . elfeed-tube-save)
         :map elfeed-search-mode-map
         ("F" . elfeed-tube-fetch)
         ([remap save-buffer] . elfeed-tube-save)))

(use-package elfeed-tube-mpv
  :straight t
  :bind (:map elfeed-show-mode-map
              ("C-c C-f" . elfeed-tube-mpv-follow-mode)
              ("C-c C-w" . elfeed-tube-mpv-where)))

(use-package syncthing
  :straight t
  :custom
  (syncthing-default-server-token "3PTShPaDUFxbdqk7k475WC5PdGKYrCfN"))

(use-package ytel
  :straight (ytel :type git :host github :repo "seblemaguer/ytel"
                  :fork (:host gitlab
                               :repo "HydraDarkNg/ytel"))
  :bind (:map ytel-mode-map
	      ("<M-return>" . ytel-show)
	      ("<RET>" . ytel-play))
  :config
  (setq ytel-invidious-api-url "http://localhost:3000"))

(use-package ytel-show
  :straight (ytel-show :type git :host github :repo "xFA25E/ytel-show"
                       :fork (:host gitlab
                                    :repo "HydraDarkNg/ytel-show"))
  :bind* (:map ytel-show-mode-map
	       ("<M-return>" . ytel-show-play)))

(require 'ytel-show)

(defun ytel--download-thumb (url)
  "Descarga archivo desde la web y devuelve su nombre como cadena
de texto"
  (let ((image-buf (url-retrieve-synchronously url)))
    (when image-buf
      (with-current-buffer image-buf
        (goto-char (point-min))
        (when (looking-at "HTTP/")
          (delete-region (point-min)
                         (progn (re-search-forward "\n[\n]+")
                                (point))))
        (buffer-substring-no-properties (point-min) (point-max))))))

(defun ytel--create-image-thumb (url)
  "Descarga URL como imagen"
  (create-image (ytel--download-thumb url) (image-type-from-file-name url) t :width 480))

(defun ytel--format-video-thumb (id)
  (let* ((format-url-thumb (format "https://img.youtube.com/vi/%s/0.jpg" id)))
    (ytel-retrieve-image format-url-thumb)))

(defun hydrang-ytel--insert-video (video)
  "Insert `VIDEO' in the current buffer."
  (insert (ytel--format-video-published (ytel-video-published video))
          " "
          (ytel--format-author (ytel-video-author video))
          " "
          (ytel--format-video-length (ytel-video-length video))
          " "
          (ytel--format-title (ytel-video-title video))
          " "
          (ytel--format-video-views (ytel-video-views video))
          "\n\n")
  (ytel--format-video-thumb (ytel-video-id video))
  (insert "\n\n"))

(defun hydrang-ytel-actual-position ()
  (interactive)
  (let ((hasta (save-excursion
                 (end-of-line)
                 (point)))
        (desde (save-excursion
                 (goto-char (point-min))
                 (point))))
    (save-restriction
      (narrow-to-region desde hasta)
      (save-excursion
        (goto-char (point-min))
        (let
            ((pos 0))
          (save-excursion
            (while
                (re-search-forward "^[[:digit:]]+" nil t)
              (setf pos (+ pos 1)))
            (setq pos-actual pos)))))
    pos-actual))

(defun hydrang-ytel-get-current-video ()
  "Get the currently selected video."
  (let ((pos (1- (hydrang-ytel-actual-position))))
    (aref ytel-videos pos)))

(defun hydrang-ytel-next-video ()
  (interactive)
  (end-of-line)
  (re-search-forward "^[[:digit:]]+" nil t)
  (beginning-of-line))

(defun hydrang-ytel-previous-video ()
  (interactive)
  (beginning-of-line)
  (re-search-backward "^[[:digit:]]+" nil t)
  (beginning-of-line))

;; (advice-add 'ytel--insert-video :override #'hydrang-ytel--insert-video)
;; (advice-add 'ytel-get-current-video :override #'hydrang-ytel-get-current-video)

(defvar hydrang-ytel-add-thumbnailsp t)

(defun hydrang-ytel-toggle-thumbnails ()
  (interactive)
  (if hydrang-ytel-add-thumbnailsp
      (progn
	(advice-remove 'ytel--insert-video #'hydrang-ytel--insert-video)
	(advice-remove 'ytel-get-current-video #'hydrang-ytel-get-current-video)
	(when ytel-search-term
	  (ytel-search ytel-search-term))
	(setq hydrang-ytel-add-thumbnailsp nil))
    (advice-add 'ytel--insert-video :override #'hydrang-ytel--insert-video)
    (advice-add 'ytel-get-current-video :override #'hydrang-ytel-get-current-video)
    (when ytel-search-term
      (ytel-search ytel-search-term))
    (setq hydrang-ytel-add-thumbnailsp t)))

(defun hydrang-ytel-show-comments ()
  (interactive)
  (let ((id (ytel-video-id (ytel-get-current-video))))
    (ytel-show-comments id)))

(evil-define-key 'normal ytel-mode-map "SPC p" #'hydrang-ytel-previous-video)
(evil-define-key 'normal ytel-mode-map "SPC S" #'ytel)
(evil-define-key 'normal ytel-mode-map "SPC n" #'hydrang-ytel-next-video)
(evil-define-key 'normal ytel-mode-map "SPC f" #'ytel-search-next-page)
(evil-define-key 'normal ytel-mode-map "SPC b" #'ytel-search-previous-page)
(evil-define-key 'normal ytel-mode-map "SPC c" #'hydrang-ytel-show-comments)
(evil-define-key 'normal ytel-mode-map "SPC T" #'hydrang-ytel-toggle-thumbnails)

(dolist (mode '(ytel-mode-hook ytel-show-mode-hook ytel-show-comments-mode-hook
		telega-chat-mode-hook telega-root-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(add-hook 'ytel-show-mode-hook #'visual-line-mode)
(add-hook 'ytel-show-comments-mode-hook #'visual-line-mode)

(defun hydrang-ytel-show-comments-translate ()
  (interactive)
  (let* ((string (hydrang-ytel-show-comments-comment-string))
	 (taker (gt-taker :text string :langs '(auto es)))
	 (translator (gt-translator :taker taker :engines (gt-google-engine)
				    :render (gt-buffer-render))))
    (gt-start
     translator)))

(defun hydrang-ytel-show-comments-comment-string ()
  (let ((start (save-excursion (goto-char (hydrang-ytel-show-comments-get-current-comment))
			       (next-line)
			       (line-beginning-position)))
	(end (hydrang-ytel-show-comments-get-next-comment)))
    (string-trim
     (replace-regexp-in-string "\\(?:\\(View [[:digit:]]+ replies\\)\\|\\(.+likes this comment\\)\\)" ""
			       (buffer-substring-no-properties start end)))))

(defun hydrang-ytel-show-comments-get-next-comment ()
  (let ((regex "\\(?:^[[:blank:]]+@.+-[[:blank:]]+[[:digit:]]\\{4\\}-[[:digit:]]\\{2\\}-[[:digit:]]\\{2\\}\\|View next page\\)")
	(pos (line-beginning-position)))
    (save-excursion
      (catch 'found
	(while (re-search-forward regex nil t)
	  (when (< pos (line-beginning-position))	
	    (throw 'found (line-beginning-position))))))))

(defun hydrang-ytel-show-comments-next-comment ()
  (interactive)
  (when-let ((pos (hydrang-ytel-show-comments-get-next-comment)))
    (goto-char pos)))

(defun hydrang-ytel-show-comments-get-previous-comment ()
  (let ((regex "\\(?:^[[:blank:]]+@.+-[[:blank:]]+[[:digit:]]\\{4\\}-[[:digit:]]\\{2\\}-[[:digit:]]\\{2\\}\\|View next page\\)")
	(pos (line-beginning-position)))
    (save-excursion
      (catch 'found
	(while (re-search-backward regex nil t)
	  (when (> pos (line-beginning-position))	
	    (throw 'found (line-beginning-position))))))))

(defun hydrang-ytel-show-comments-previous-comment ()
  (interactive)
  (when-let ((pos (hydrang-ytel-show-comments-get-previous-comment)))
    (goto-char pos)))

(define-key ytel-show-comments-mode-map "n" #'hydrang-ytel-show-comments-next-comment)
(define-key ytel-show-comments-mode-map "p" #'hydrang-ytel-show-comments-previous-comment)

(defun hydrang-ytel-show-comments-get-current-comment ()
  (let ((regex "\\(?:^[[:blank:]]+@.+-[[:blank:]]+[[:digit:]]\\{4\\}-[[:digit:]]\\{2\\}-[[:digit:]]\\{2\\}\\)")
	(pos (line-beginning-position)))
    (save-excursion
      (goto-char (point-min))
      (catch 'found
	(while (re-search-forward regex nil t)
	  (when (< pos (line-beginning-position))
	    (beginning-of-line)
	    (re-search-backward regex nil t)
	    (throw 'found (line-beginning-position))))))))

(defun hydrang-ytel-show-comments-next-page ()
  (interactive)
  (when-let ((continuation (cdr (ytel-show-comments--comments-data ytel-show-comments--video-id))))
    (ytel-show-comments--add-to-history ytel-show-comments--video-id continuation)
    (ytel-show-comments-revert-buffer)))

(define-key ytel-show-comments-mode-map "f" #'hydrang-ytel-show-comments-next-page)
(define-key ytel-show-comments-mode-map "b" #'ytel-show-comments-previous-page)

(use-package imenu
  :bind ([remap imenu] . consult-imenu))

(use-package imenu-list
  :straight t
  :bind (:map org-mode-map
         ("M-g l" . imenu-list)))

(use-package mpv
  :straight t)

(defun ytel-play ()
  (interactive)
  (let* ((video (ytel-get-current-video))
         (id (ytel-video-id video)))
    (mpv-play-url (concat "https://youtu.be/" id))))

(defun ytel-show-play ()
  (interactive)
  (let* ((id (ytel-show--current-video-id)))
    (mpv-play-url (concat "https://youtu.be/" id))))


(defun ytel-play-audio ()
  (interactive)
  (let* ((video (ytel-get-current-video))
         (id (ytel-video-id video)))
    (emms-play-url (concat "https://youtu.be/" id))))

(use-package ytdl
  :straight (:type git :host gitlab :repo "tuedachu/ytdl")
  :custom
  (ytdl-command (expand-file-name "~/bin/yt-dlp"))
  (ytdl-video-extra-args '("--format" "bestvideo[height<=1080]+bestaudio/best[height<=1080]")))

(defun ytel-download ()
  (interactive)
  (let* ((video (ytel-get-current-video))
         (id (ytel-video-id video))
         (title (ytel-video-title video))
         (author (ytel-video-author video)))
    (ytdl--download-async (concat "https://youtu.be/" id)
                          (concat "~/Descargas/" title)
			  "")))

(defun ytel-show-download ()
  (interactive)
  (let* ((id (ytel-show--current-video-id))
         (title (assoc-default
                 'title
                 (ytel--API-call-synchronously (concat "videos/" id) '()))))
    (ytdl--download-async (concat "https://youtu.be/" id)
                          (concat "~/Descargas/" title)
                          "")))

(defun ytel-browser ()
  (interactive)
  (let* ((video (ytel-get-current-video))
         (id (ytel-video-id video)))
    (browse-url (concat "https://youtu.be/" id))))

(defun ytel--API-call-synchronously (method args)
  "Perform a call to the invidious API method METHOD passing ARGS.

Curl is used to perform the request.  An error is thrown if it exits with a non
zero exit code otherwise the request body is parsed by `json-read' and returned."
  (with-temp-buffer
    (let ((exit-code (call-process "curl" nil t nil
				   "--silent"
				   "-X" "GET"
				   (concat ytel-invidious-api-url
					   "/api/v1/" method
					   "?" (url-build-query-string args)))))
      (unless (= exit-code 0)
	(error "Curl had problems connecting to Invidious"))
      (goto-char (point-min))
      (json-read))))
