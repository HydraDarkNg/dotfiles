(defun my-modeline--mode-name ()
  (capitalize (symbol-name major-mode)))

(defvar writer-mode nil)

(defun writer-mode ()
  (interactive)
  (if writer-mode
      (setq-local writer-mode nil)
    (setq-local writer-mode t)))

(defun hydrang-modeline--count-words ()
  (if writer-mode
      (if mark-active
          (format "Palabras: %s" (count-words (region-beginning) (region-end)))
        (format "Palabras: %s" (count-words (point-min) (point-max))))))

(defvar-local hydrang-modeline-as-changed
    '(:eval (propertize
             (if buffer-read-only
                 "$"
               (if (buffer-modified-p)
                   "*"
                 "-"))
             'face 'shadow)))

(defvar-local my-modeline-major-mode
    '(:eval
      (propertize (my-modeline--mode-name) 'face 'bold)))

(defvar-local my-modeline-buffer-name
    '(:eval (propertize
             (let ((name
                    (cl-case major-mode
                      (org-mode (or (org-get-title) (buffer-name)))
                      (t (buffer-name)))))
               (if (< 35 (length name))
                   (format "%s..." (cl-subseq name 0 32))
                 name))
             'face '(:inherit modus-themes-fg-magenta :weight bold))))

(defvar-local my-modeline-count-words
    '(:eval (when (hydrang-modeline--count-words)
              (propertize (hydrang-modeline--count-words)
                          'face '(:inherit link :slant italic :weight bold :underline nil)))))

(use-package keycast
  :straight t
  :custom
  (keycast-mode-line-insert-after 'my-modeline-major-mode))

(defcustom hydrang--right-mode-line-format
 '((when org-timer-mode-line-timer
    org-timer-mode-line-string)
   " "
   hammy-mode-lighter
   " "
   org-pomodoro-mode-line)
 "")

(defun hydrang-right-mode-line-format ()
  (cl-remove-if #'null
		(mapcar #'eval (reverse hydrang--right-mode-line-format))))

(defcustom hydrang-modeline-format
  '("%e "
    hydrang-modeline-as-changed
    " "
    my-modeline-buffer-name
    " "
    my-modeline-major-mode
    " "
    mode-line-position
    " "
    my-modeline-count-words
    " "
    mode-line-format-right-align
    (:eval (hydrang-right-mode-line-format)))
  "")

(setq-default mode-line-format hydrang-modeline-format)

(put 'my-modeline-fringe 'risky-local-variable t)
(put 'my-modeline-buffer-name 'risky-local-variable t)
(put 'my-modeline-major-mode 'risky-local-variable t)
(put 'hydrang-modeline-as-changed 'risky-local-variable t)
(put 'my-modeline-count-words 'risky-local-variable t)
(put 'hydrang-right-mode-line-format 'risky-local-variable t)

(provide 'hydrang-modeline)
