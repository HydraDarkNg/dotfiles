;; hydrang-org.el --- Mis configuraciones personales para el org-mode -*- lexical-binding: t -*-

;; Copyright (C) 2023-2024 HydraDarkNg

;; Autor: HydraDarkNg
;; URL: https://hydradarkng
;; Version: 0.0.1
;; Package-Requires: ((emacs "30.0.5"))

;; Este archivo no es parte de GNU Emacs.

;; Este archivo es Software libre tu puede redistribuirlo y/o modificarlo
;; esta bajo los términos de la GNU General Public License publicada por
;; la Free Software Fundation, ya sea la versión 3 de esta licencia, o
;; (según tu opinión) cualquier versión posterior.

;; Este programa es distribuido con la esperanza de que puedas usarlo,
;; pero NO VIENE CON NINGUNA GARANTÍA; sin siquiera la garantía implícita de
;; COMERCIALIZACIÓN o APTITUD PARA UN PROPÓSITO PARTICULAR. Vea la GNU
;; General Public License para obtener más detalles.
;;
;; Debería haber obtenido una copia de la GNU General Public License
;; junto con este programa. Si no, mira <https://www.gnu.org/licenses/>.

;; Comentario:
;;
;; Este archivo cubre todas mis configuraciones sobre el Org,
;; esto significa que puedes usarlo en mis configuraciones de
;; GNU Emacs:

;; Recuerda que cada parte de código de Elisp lo escribí yo mismo
;; con fines educativos y recreativos. No soy programador, por lo
;; que no recomendaría que copies cualquier parte de código sin estar
;; seguro de que es lo que hace.

;; Código:

(defgroup hydrang-org ()
  "Configuraciones para org.el."
  :group 'org)

;; Latex preview

(use-package org
  :defer
  :straight `(org
              :fork (:host nil
                     :repo "https://git.tecosaur.net/tec/org-mode.git"
                     :branch "dev"
                     :remote "tecosaur")
              :files (:defaults "etc")
              :build t
              :pre-build
              (with-temp-file "org-version.el"
               (require 'lisp-mnt)
               (let ((version
                      (with-temp-buffer
                        (insert-file-contents "lisp/org.el")
                        (lm-header "version")))
                     (git-version
                      (string-trim
                       (with-temp-buffer
                         (call-process "git" nil t nil "rev-parse" "--short" "HEAD")
                         (buffer-string)))))
                (insert
                 (format "(defun org-release () \"The release version of Org.\" %S)\n" version)
                 (format "(defun org-git-version () \"The truncate git commit hash of Org mode.\" %S)\n" git-version)
                 "(provide 'org-version)\n")))
              :pin nil)
  :bind
  ("C-c m" . org-maxima-entry-insert)
  :general
  (:states 'normal
	   :keymaps 'org-mode-map
	   "TAB" 'org-cycle
	   "SPC v t" 'org-babel-tangle
	   "SPC '" 'org-edit-special
	   "SPC c o" 'org-open-at-point
	   "SPC c c" 'org-ctrl-c-ctrl-c)
  (:states 'normal
	   :keymap 'org-src-mode-map
	   "SPC '" 'org-edit-src-exit)
  :hook
  (org-mode . org-indent-mode)
  :custom
  (org-export-allow-bind-keywords t)
  (org-startup-with-inline-images t)
  (org-ellipsis "…")
  (org-image-actual-width 600)
  (org-agenda-tags-column 10)
  (org-agenda-block-separator ?═)
  (org-agenda-time-grid
   '((weekly today require-timed)
     (800 1000 1200 1400 1600 1800 2000)
     " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
   org-agenda-current-time-string
   "⇐ Jetzt ═══════════════════")
  (org-hide-emphasis-markers t)
  (org-pretty-entities t)
  (org-auto-align-tags nil)
  (org-startup-with-latex-preview t)
  (org-tags-column -1)
  (org-preview-latex-image-directory (expand-file-name
                                      "~/.cache/ltximg/"))
  (org-latex-preview-ltxpng-directory (expand-file-name	
                                       "~/.cache/ltximg/"))
  (org-format-latex-options (plist-put org-format-latex-options :scale 1.25))
  (org-directory (expand-file-name "~/Documentos/Org/"))
  (org-startup-folded t)
  (org-clock-sound (concat *dotfiles* "sounds/alarma.mp3"))
  (org-agenda-window-setup 'only-window)
  :config
  (require 'ox-md)
  (require 'org-inlinetask)
  (require 'org-capture)
  ;; (require 'ob-ledger)
  (require 'org-habit)
  (require 'org-protocol))

;; (setf (cdr (assoc 'file org-link-frame-setup)) 'find-file)

(use-package org-latex-preview
  :config
  ;; Increase preview width  
  (plist-put org-latex-preview-appearance-options
             :zoom 1.25)

  ;; Use dvisvgm to generate previews
  ;; You don't need this, it's the default:
  (setq org-latex-preview-process-default 'dvisvgm)
  
  ;; Turn on auto-mode, it's built into Org and much faster/more featured than
  ;; org-fragtog. (Remember to turn off/uninstall org-fragtog.)
  (add-hook 'org-mode-hook 'org-latex-preview-auto-mode)

  ;; Block C-n, C-p etc from opening up previews when using auto-mode
  (setq org-latex-preview-auto-ignored-commands
        '(next-line previous-line mwheel-scroll
          scroll-up-command scroll-down-command))

  ;; Enable consistent equation numbering
  (setq org-latex-preview-numbered t)

  ;; Bonus: Turn on live previews.  This shows you a live preview of a LaTeX
  ;; fragment and updates the preview in real-time as you edit it.
  ;; To preview only environments, set it to '(block edit-special) instead
  (setq org-latex-preview-live t)

  ;; More immediate live-previews -- the default delay is 1 second
  (setq org-latex-preview-live-debounce 0.25)
  (push '("" "amsfonts" t ("pdflatex")) org-latex-default-packages-alist)
  (push '("" "tikz" t ("pdflatex")) org-latex-default-packages-alist)
  (push '("" "amsmath" t ("pdflatex")) org-latex-default-packages-alist)
  (push '("" "amssymb" t ("pdflatex")) org-latex-default-packages-alist)
  (setq org-latex-default-packages-alist (cl-remove-duplicates org-latex-default-packages-alist :test #'equal)))

;; Matemáticas:

(defun org-maxima-entry-insert (input)
  "Recibe un INPUT como una instrucción de maxima,
y devuelve el resultado de esta intrucción en el
formato TeX."
  (interactive "sMaxima: ")
  (maxima-single-string-wait
   (maxima-strip-string-add-semicolon (format "tex(%s)$" input)))
  (insert (s-trim (maxima-last-output-noprompt maxima-inferior-process))))

;; Aspecto visual:

(use-package olivetti
  :straight t
  :hook
  (org-mode . olivetti-mode)
  :custom 
  (olivetti-body-width 90)
  :config
  (set-face-attribute 'olivetti-fringe nil :background (face-attribute 'default :background)))

(use-package org-bullets
  :straight t
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org-fancy-priorities
  :straight t
  :defines org-fancy-priorities-list
  :hook (org-mode . org-fancy-priorities-mode)
  :config
  (setq org-fancy-priorities-list '((?A . "🔴")
                                    (?B . "🟠")
                                    (?C . "🟡")
                                    (?D . "🟢"))))

(use-package org-pretty-tags
  :straight t
  :config
  (setq org-pretty-tags-surrogate-strings
        '(("@house" . "🏠")
          ("@school" . "🏫")
          ("@park" . "🏞️")
          ("@work" . "🧑🏽‍🏫")
          ("@healt" . "🏃")
          ("@obligation" . "🧑🏾‍🔧")
          ("@study" . "🧑🏽‍🎓")
          ("@personal" . "🧒🏽")))
  (org-pretty-tags-global-mode))

;; Configuraciónes generales:

(use-package org-contrib
  :straight t)

(setq org-habit-graph-column 50)

;;; Parche para org-clock-sound

;; No me funciona bien aplay, así que la redefiní
;; para que use mpv.

(defun org-clock-play-sound (&optional clock-sound)
  "Play sound as configured by `org-clock-sound'.
Use alsa's aplay tool if available.
If CLOCK-SOUND is non-nil, it overrides `org-clock-sound'."
  (let ((org-clock-sound (or clock-sound org-clock-sound)))
    (cond
     ((not org-clock-sound))
     ((eq org-clock-sound t) (beep t) (beep t))
     ((stringp org-clock-sound)
      (let ((file (expand-file-name org-clock-sound)))
        (if (file-exists-p file)
            (if (executable-find "mpv")
                (start-process "org-clock-play-notification" nil
                               "mpv" file)
              (condition-case nil
                  (play-sound-file file)
                (error (beep t) (beep t))))))))))

;; Programación:

(require 'ob-C)

(use-package gnuplot
  :straight t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((maxima . t)
   (gnuplot . t)
   (ledger . t)
   (lisp . t)
   (C . t)
   (python . t)
   (ditaa . t)))

(setq org-babel-lisp-eval-fn #'sly-eval)

;; Escuela:

(defvar study-mode nil)

(defun study-mode ()
  "Incicia una serie de configuraciones que hacen el
texto más legible cuando estas estudiando."
  (interactive)
  (if study-mode
      (progn
        (set-face-attribute 'default nil :height 150)
        (setq-local mode-line-format nil)
        (setq study-mode nil))
    (set-face-attribute 'default nil :height 400)
    (setq study-mode t)))

(use-package org-drill
  :straight t)

(use-package org-pomodoro
  :straight t
  :custom
  (org-pomodoro-length 25)
  (org-pomodoro-short-break-length 5)
  (org-pomodoro-long-break-length 10)
  (org-pomodoro-long-break-frequency 3)
  (org-pomodoro-start-sound-p t)
  (org-pomodoro-audio-player "mpv")
  (org-pomodoro-finished-sound (concat *dotfiles*
                                       "sounds/alarma.mp3")))

;; Org Agenda:

(defcustom hydrang-org-organize-directory
  (expand-file-name org-directory)
  "Es mi directorio de los archivos de mis archivos,
de organización."
  :type 'directory
  :group 'hydrang-org)

(defmacro hydrang-org-organize-file (file)
  "Esta macro devuelve un archivo de Org del
 directorio `hydrang-org-organize-directory'."
  `(concat hydrang-org-organize-directory (symbol-name ',file) ".org"))

(defmacro hydrang-org-organize-files (&rest files)
  "Esta macro devuelve una lista de archivos Org del
directorio `hydrang-org-organize-directory'."
  `(mapcar
    (lambda (file)
      (concat hydrang-org-organize-directory (if (stringp file)
                                                 file
                                               (symbol-name file))
              ".org"))
    ',files))

(add-hook 'org-agenda-mode-hook (lambda () (setq-local mode-line-format nil)))

(defgroup hydrang-organize ()
  "Configurations for ")

(defcustom hydrang-organize-file (hydrang-org-organize-file Life)
  "")

(defun hydrang-agenda-promt ()
  (let ((name (completing-read
               (format-prompt "Agenda" nil)
               (mapcar #'file-name-base org-agenda-files))))
    (concat hydrang-org-organize-directory name ".org")))

(defun hydrang-agenda (file)
  (interactive (list (hydrang-agenda-promt)))
  (find-file file))

(defun hydrang-calendar-prompt ()
  (let* ((org-agenda-files (list hydrang-org-calendar-directory))
         (name (completing-read
                (format-prompt "Agenda" nil)
                (mapcar #'file-name-base (org-agenda-files)))))
    (concat hydrang-org-calendar-directory name ".org")))

(defun hydrang-calendar (file)
  (interactive (list (hydrang-calendar-prompt)))
  (find-file file))

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)" "CANC(c!)")))

;; Org Refile:

(defcustom hydrang-org-agenda-refile
  (hydrang-org-organize-file Projects)
  "Mi file for refile my "
  :type 'path
  :group 'hydrang-org)

(setq org-refile-targets '((hydrang-org-agenda-refile :maxlevel . 1)))

;; Org Capture

;; (require 'mu4e-org)

;; (setq org-capture-templates
;;       `(("t" "Nota o pendiente" entry (file ,(denote-find-by-title "Inbox"))
;;          "* %?\n %i\n")
;;         ("m" "Mail" entry (file+olp ,(hydrang-org-organize-file Inbox) "Read later")
;;          "* TODO Read [[mu4e:msgid:%:message-id][%:subject]]\n")))

(use-package org-super-agenda
  :straight t
  :init
  (org-super-agenda-mode 1))

(use-package hammy
  :straight (:type git :host github :repo "alphapapa/hammy.el"))

(use-package org-youtube
  :straight (:type git :host gitlab :repo "HydraDarkNg/org-youtube")
  :hook
  (org-mode . org-youtube-mode))

(use-package org-xournalpp
  :straight (:host gitlab :repo "vherrmann/org-xournalpp")
  :custom
  (org-xournalpp-image-type 'png)
  (org-xournalpp--dir (concat straight-base-dir "straight/repos/org-xournalpp/")))

(use-package ledger-mode
  :straight t)

;; Org Agenda Views

(defun hydrang-org-agenda-only-activities-today ()
  (let ((today (rx-to-string (format-time-string "<%Y-%m-%d %a")))
        (subtree-end (save-excursion (org-end-of-subtree t))))
    (unless (and (re-search-forward today subtree-end t)
		 (not (re-search-forward "habit" subtree-end t)))
      subtree-end)))

;; (defcustom hydrang-org-custom-calendar-agenda
;;   `((agenda "" ((org-agenda-time-grid nil)
;;                 (org-agenda-files ',(append (hydrang-org-calendar-files)
;;                                             org-agenda-files))
;;                 (org-agenda-span 'day)
;;                 (org-agenda-overriding-header "\nMi calendario de hoy\n")
;; 		(org-super-agenda-groups
;; 		 '((:name "Habits"
;; 		    :habit t))))))
;;   "Mi vista en forma de calendario de Time Blocking."
;;   :type 'list
;;   :group 'hydrang-org)

;; (defcustom hydrang-org-custom-daily-agenda
;;   `((todo "" ((org-agenda-overriding-header "Mis actividades de hoy\n")
;;               (org-agenda-skip-function 'hydrang-org-agenda-only-activities-today)))
;;     (todo "" ((org-agenda-files ',(hydrang-org-organize-files Inbox "Mobile Inbox"))
;;               (org-agenda-overriding-header "\nMi bandeja de entrada\n")))
;;     ,@hydrang-org-custom-calendar-agenda)
;;   "Custom agenda for use in `org-agenda-custom-commands'."
;;   :type 'list
;;   :group 'hydrang-org)

(use-package bluetooth
  :straight t)

;; (setq org-agenda-custom-commands
;;       `(("i" "Inbox"
;;          ((todo "" ((org-agenda-files
;;                      '("~/Documentos/Org/Organize/Inbox.org"))))))
;;         ("T" "Time Blocking"
;;          ,hydrang-org-custom-calendar-agenda)
;;         ("A" "Agenda"
;;          ,hydrang-org-custom-daily-agenda)))

;; Pequeños retoques a mi org agenda
(setf calendar-day-name-array
      ["Domingo" "Lunes" "Martes" "Miércoles" "Jueves" "Viernes" "Sábado"]
      calendar-month-name-array
      ["Enero" "Febrero" "Marzo" "Abril" "Mayo" "Junio" "Julio" "Agosto"
       "Septiembre" "Octubre" "Noviembre" "Diciembre"])

(defun hydrang-org-agenda-format-date (date)
  "Esta función es el formato que tendrán
las fechas en mi org agenda."
  (-let* (((month day year) date)
	  (day-name (calendar-day-name date))
          (month-name (calendar-month-name month)))
    (message "%s" date)
    (format "%s %d de %s del %s"
            day-name day month-name year)))

(setq org-agenda-format-date #'hydrang-org-agenda-format-date)

;; Time block display

(use-package org-timeblock
  :straight t
  :config
  (setq org-timeblock-files (list (concat org-directory "Calendar.org"))
	org-timeblock-span 7))

;; Open link files

(push
 '("\\.png" . "feh %s")
 org-file-apps)

(push
 '("\\.ggb" . "flatpak run org.geogebra.GeoGebra %s")
 org-file-apps)

(provide 'hydrang-org)

;; hydrang-org.el termina aquí.
