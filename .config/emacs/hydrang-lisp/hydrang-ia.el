;; hydrang-ia.el --- Mis configuraciones personales para el el uso de inteligencia artificial en emacs -*- lexical-binding: t -*-

;; Copyright (C) 2023-2024 HydraDarkNg

;; Autor: HydraDarkNg
;; URL: https://hydradarkng
;; Version: 0.0.1
;; Package-Requires: ((emacs "30.0.5"))

;; Este archivo no es parte de GNU Emacs.

;; Este archivo es Software libre tu puede redistribuirlo y/o modificarlo
;; esta bajo los términos de la GNU General Public License publicada por
;; la Free Software Fundation, ya sea la versión 3 de esta licencia, o
;; (según tu opinión) cualquier versión posterior.

;; Este programa es distribuido con la esperanza de que puedas usarlo,
;; pero NO VIENE CON NINGUNA GARANTÍA; sin siquiera la garantía implícita de
;; COMERCIALIZACIÓN o APTITUD PARA UN PROPÓSITO PARTICULAR. Vea la GNU
;; General Public License para obtener más detalles.
;;
;; Debería haber obtenido una copia de la GNU General Public License
;; junto con este programa. Si no, mira <https://www.gnu.org/licenses/>.

;; Comentario:
;;
;; Este archivo cubre todas mis configuraciones sobre el Org,
;; esto significa que puedes usarlo en mis configuraciones de
;; GNU Emacs:

;; Recuerda que cada parte de código de Elisp lo escribí yo mismo
;; con fines educativos y recreativos. No soy programador, por lo
;; que no recomendaría que copies cualquier parte de código sin estar
;; seguro de que es lo que hace.

;; Código:


(use-package gptel
  :straight t
  :defer t
  :commands gptel-send
  :config
  (setq-default
   gptel-model "gemini-pro"
   gptel-backend (gptel-make-gemini "Gemini"
                   :key (lookup-password :host "gemini")
                   :stream t)
   gptel-default-mode #'org-mode)
  :init
  (general-define-key
   :prefix "SPC ia"
   :states '(normal visual)
   "RET" 'gptel-send
   "m RET" 'gptel-menu))

(use-package gptel-quick
  :after gptel
  :straight (:host github :repo "karthink/gptel-quick"))

(use-package go-translate
  :straight t
  :config
  (setq gt-default-translator
      (gt-translator
       :taker   (gt-taker :text 'buffer :pick 'paragraph)  ; config the Taker
       :engines (gt-google-engine)))                       ; config the Engines
  (setq gt-langs '(auto es)))

(provide 'hydrang-ia)
