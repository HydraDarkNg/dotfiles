(use-package nov
  :straight t
  :mode ("\\.epub\\'" . nov-mode)
  :hook
  (nov-mode . olivetti-mode)
  (nov-mode . visual-line-mode))

(setq nov-variable-pitch nil
      nov-text-width t)

(defun hydrang-books-face ()
  (setq buffer-face-mode-face '(:height 250))
  (setq-local olivetti-body-width 20)
  (buffer-face-mode)
  (setq-local global-hl-line-mode nil))

(add-hook 'nov-mode-hook #'hydrang-books-face)


(defcustom hydrang-books-directory
  (expand-file-name "Resources/Books/" org-directory)
  "This is the directory where I have my books or article documents.")

(defcustom hydrang-books-history
  '()
  "")

(defcustom hydrang-max-authors-length 20
  "The max length of the author name")

(defun hydrang-print-author-text (author)
  (if (<= (length author)
	  hydrang-max-authors-length)
      (concat author
	      (make-string (- (1+ hydrang-max-authors-length)
			      (length author))
			   32))
    (concat (seq-subseq author 0 hydrang-max-authors-length) " ")))

(defun hydrang-books-directory-files ()
  (let ((denote-directory hydrang-books-directory))
    (mapcar (lambda (file)
	      (propertize
	       (concat
		(propertize (hydrang-print-author-text (capitalize (replace-regexp-in-string "=" " " (or (denote-retrieve-filename-signature file) "Not author"))))
			    'face 'font-lock-keyword-face)
		" "
		(propertize (capitalize (replace-regexp-in-string "-" " " (denote-retrieve-filename-title file)))
			    'face '(:inherit font-lock-escape-face :weight bold)))
	       'path file))
	    (denote-directory-files (rx "." (or "pdf" "epub" "djvu") line-end)))))

(defun hydrang-book-path (string)
  (get-text-property 0 'path string))

(defun hydrang-consult-books ()
  (interactive)
  (let* ((items (hydrang-books-directory-files))
	 (selection (consult--read
		     items
		     :history 'hydrang-books-history
		     :prompt "Resource: "))
	 result)
    (when-let* ((_ selection)
		(item (catch 'found
			(dolist (item items result)
			  (when (string= item selection)
			    (setq result item)
			    (throw 'found result))))))
      (find-file (hydrang-book-path item)))))

(provide 'hydrang-books)
