(setq gamegrid-glyph-height-mm 5.5)

(use-package chess
  :straight t
  :custom
  (chess-default-display '(chess-images chess-ics1 chess-plain))
  (chess-images-default-size 80)
  (chess-stockfish-path "~/Descargas/stockfish/stockfish-ubuntu-x86-64-avx2")
  (chess-ai-depth 40)
  :config
  (setq chess-gnuchess-regexp-alist
        (list
         (cons (concat "Mi jugada : \\(" chess-algebraic-regexp "\\)")
               (function
                (lambda ()
                  (funcall chess-engine-response-handler 'move
                           (chess-engine-convert-algebraic (match-string 1) t)))))
         (cons "Movimiento no válido:"
               (function
                (lambda ()
                  (chess-error 'illegal-move))))
         (cons "Tablero incorrecto"
               (function
                (lambda ()
                  ;; gnuchess didn't like the given position, which
                  ;; means it won't play against it unless we send a
                  ;; "go" after the user's move
                  (setq chess-gnuchess-bad-board t)))))))

(use-package pygn-mode
  :straight t
  :mode ("\\.pgn\\'" . pygn-mode)
  :config
  (setq pygn-mode-python-executable "python3"
        pygn-mode-board-size 800
        pygn-mode-board-flipped nil)

  (define-key pygn-mode-map (kbd "M-<left>") #'pygn-mode-previous-move-follow-board)
  (define-key pygn-mode-map (kbd "M-<right>") #'pygn-mode-next-move-follow-board))

(use-package speed-type
  :straight t)

(provide 'hydrang-games)
