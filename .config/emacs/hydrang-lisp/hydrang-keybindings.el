;; hydrang-keybindings.el --- Mis configuraciones personales para el teclado -*- lexical-binding: t -*-

;; Copyright (C) 2023-2024 HydraDarkNg

;; Autor: HydraDarkNg
;; URL: 
;; Version: 0.0.1
;; Package-Requires: ((emacs "30.0.5"))

;; Este archivo no es parte de GNU Emacs.

;; Este archivo es Software libre tu puede redistribuirlo y/o modificarlo
;; esta bajo los términos de la GNU General Public License publicada por
;; la Free Software Fundation, ya sea la versión 3 de esta licencia, o
;; (según tu opinión) cualquier versión posterior.

;; Este programa es distribuido con la esperanza de que puedas usarlo,
;; pero NO VIENE CON NINGUNA GARANTÍA; sin siquiera la garantía implícita de
;; COMERCIALIZACIÓN o APTITUD PARA UN PROPÓSITO PARTICULAR. Vea la GNU
;; General Public License para obtener más detalles.
;;
;; Debería haber obtenido una copia de la GNU General Public License
;; junto con este programa. Si no, mira <https://www.gnu.org/licenses/>.

;; Comentario:
;;
;; Este archivo cubre todas mis configuraciones sobre el Org,
;; esto significa que puedes usarlo en mis configuraciones de
;; GNU Emacs:

;; Recuerda que cada parte de código de Elisp lo escribí yo mismo
;; con fines educativos y recreativos. No soy programador, por lo
;; que no recomendaría que copies cualquier parte de código sin estar
;; seguro de que es lo que hace.

;; Código:

(use-package evil
  :straight t
  :config
  (evil-mode t))

(use-package general
  :straight t)

(use-package transient
  :straight t)

(general-create-definer emacs-commands
  :states 'normal
  :prefix "SPC")

(emacs-commands
  "f" 'find-file
  "b" 'switch-to-buffer
  "s" 'isearch-forward
  "S" 'save-buffer
  "x" 'execute-extended-command
  "d" 'dired
  "o" 'other-window
  "0" 'delete-window
  "1" 'delete-other-windows
  "2" 'split-window-below
  "3" 'split-window-right
  "k" 'kill-buffer)

;; (transient-define-prefix transient--global-ctrl-c ()
;;   ["Ctrl c commands"
;;    ("s" "Save buffer" save-buffer)])

;; (emacs-commands
;;   "c" 'transient--global-ctrl-c)

(transient-define-prefix transient--elisp ()
  ["Eval commands"
   ("d" "Eval defun" eval-defun)
   ("r" "Eval region" eval-region)])

(evil-define-key '(normal visual) emacs-lisp-mode-map (kbd "SPC e") #'transient--elisp)

(provide 'hydrang-keybindings)
