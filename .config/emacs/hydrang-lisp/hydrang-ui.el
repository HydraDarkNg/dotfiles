(global-hl-line-mode)
(setq backup-inhibited t
      make-backup-files nil
      create-lockfiles nil)

(setq display-line-numbers 'relative)
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode t)

(setq-default cursor-type 'bar)
(when (eq system-type 'gnu/linux)
  (scroll-bar-mode -1))
(tool-bar-mode -1)
(menu-bar-mode -1)
(show-paren-mode 1)
(electric-pair-mode 1)
(blink-cursor-mode 0)

;; (set-frame-parameter (selected-frame) 'fullscreen 'fullscreen)

(use-package fontaine
  :straight t
  :custom
  (fontaine-presets
   '((regular
      :default-family "Iosevka Nerd Font"
       :default-height 150)))
  :config
  (fontaine-set-preset 'regular))

(use-package spacious-padding
  :straight t
  :custom
  (spacious-padding-widths
   '(:internal-border-width 15
     :header-line-width 5
     :mode-line-width 5
     :tab-width 4
     :right-divider-width 30
     :scroll-bar-width 8
     :fringe-width 8)))


(use-package modus-themes
  :straight t
  :config
  (setq modus-themes-headings
	'((0 . (1.8))
	  (1 . (1.5))
          (2 . (1.4))
	  (3 . (1.3))
	  (4 . (1.2))
	  (5 . (1.1)))))

(use-package ef-themes
  :straight (:type git :host github :repo "protesilaos/ef-themes")
  :config
  (setq ef-themes-headings
	'((0 . (1.8))
	  (1 . (1.5))
          (2 . (1.4))
	  (3 . (1.3))
	  (4 . (1.2))
	  (5 . (1.1)))))

(use-package dracula-theme
  :straight t
  :defer t
  :config
  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.8)
                  (org-level-2 . 1.5)
                  (org-level-3 . 1.4)
                  (org-level-4 . 1.3)
                  (org-level-5 . 1.2)
                  (org-level-6 . 1.1)))
    (set-face-attribute (car face) nil :height (cdr face))))

(mapc #'disable-theme custom-enabled-themes)
(load-theme 'ef-autumn :no-confirm)
(spacious-padding-mode t)

(use-package doom-themes
  :straight t)

(use-package nerd-icons
  :straight t)

;; Configuraciones menores:

;;(require 'shr)
;; (set-face-attribute 'shr-text nil :font "Iosevka Nerd Font")

(dolist (mode '(org-mode-hook
                org-agenda-mode-hook
                nov-mode-hook
                term-mode-hook
                shell-mode-hook
                markdown-mode-hook
                vterm-mode-hook
                pdf-view-mode-hook
                treemacs-mode-hook
                dired-mode-hook
                eww-mode-hook
                eshell-mode-hook
                emms-playlist-mode-hook
                emms-browser-mode-hook
                elfeed-show-mode-hook
                eat-mode-hook
                org-timeblock-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(provide 'hydrang-ui)
