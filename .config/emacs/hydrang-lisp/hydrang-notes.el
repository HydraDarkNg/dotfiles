;; hydrang-notes.el --- Mis configuraciones personales para mis notas -*- lexical-binding: t -*-

;; Copyright (C) 2023-2024 HydraDarkNg

;; Autor: HydraDarkNg
;; URL: https://hydradarkng
;; Version: 0.0.1
;; Package-Requires: ((emacs "30.0.5"))

;; Este archivo no es parte de GNU Emacs.

;; Este archivo es Software libre tu puede redistribuirlo y/o modificarlo
;; esta bajo los términos de la GNU General Public License publicada por
;; la Free Software Fundation, ya sea la versión 3 de esta licencia, o
;; (según tu opinión) cualquier versión posterior.

;; Este programa es distribuido con la esperanza de que puedas usarlo,
;; pero NO VIENE CON NINGUNA GARANTÍA; sin siquiera la garantía implícita de
;; COMERCIALIZACIÓN o APTITUD PARA UN PROPÓSITO PARTICULAR. Vea la GNU
;; General Public License para obtener más detalles.
;;
;; Debería haber obtenido una copia de la GNU General Public License
;; junto con este programa. Si no, mira <https://www.gnu.org/licenses/>.

;; Comentario:
;;
;; Este archivo cubre todas mis configuraciones sobre el Org,
;; esto significa que puedes usarlo en mis configuraciones de
;; GNU Emacs:

;; Recuerda que cada parte de código de Elisp lo escribí yo mismo
;; con fines educativos y recreativos. No soy programador, por lo
;; que no recomendaría que copies cualquier parte de código sin estar
;; seguro de que es lo que hace.

;; Código:


(use-package denote
  :straight (denote :type git :host github :repo "protesilaos/denote")
  :hook
  (dired-mode . denote-dired-mode)
  :custom
  (denote-directory (concat hydrang-org-organize-directory "Notas"))
  (denote-file-type 'org)
  (denote-known-keywords '("zettelkasten" "video" "book"
                           "extract" "cite" "reflection"
                           "flashcard" "glossary" "summary"))
  :config
  (require 'denote-org-dblock))

(use-package consult-notes
  :straight t
  :bind ("C-c n n" . consult-notes)
  :custom
  (consult-notes-denote-files-function (lambda ()
                                         (denote-directory-files "\\.org")))
  :init
  (consult-notes-denote-mode))

(defgroup hydrang-notes ()
  "Eso abarcara todos mis apuntes, tanto a mano con xournal
como en el org-mode de la escuela."
  :group 'denote)

(defun denote-retrieve-keywords-value-as-string (file file-type)
  (mapconcat #'identity (denote-retrieve-keywords-value file file-type) " "))

(defun org-dblock-write:hydrang-extracts (params)
  (let* ((note (denote-retrieve-filename-identifier (buffer-file-name)))
         (denote-directory (concat denote-directory "/Extracts/"))
         (pos (point))
         notes)
    (dolist (file (denote-directory-files ".org"))
      (with-temp-buffer
        (insert-file file)
        (let ((buffer (org-element-parse-buffer)))
          (when (string= note (org-element-property :EXTRACT_OF buffer))
            (push file notes)))))
    (when notes
     (insert "|-\n|Note|Type|\n|-\n")
     (insert (mapconcat (lambda (file)
                          (format "| [[denote:%s][%s]] | %s |"
				  (denote-retrieve-filename-identifier file)
                                  (let ((title (denote-retrieve-title-value file 'org)))
                                    (if (< 30 (length title))
                                        (format "%s..." (cl-subseq title 0 27))
                                      title))
                                  (denote-retrieve-keywords-value-as-string file 'org)))
                        (reverse (denote-sort-files notes 'identifier))
                        "\n"))
     (insert "\n|-")
     (goto-char pos)
     (org-table-align))))

(defun hydrang-notes-bibliography ()
  (interactive)
  (let ((denote-directory (concat denote-directory "/Bibliography")))
    (consult-notes)))

(defun hydrang-notes-extracts ()
  (interactive)
  (let ((denote-directory (concat denote-directory "/Extracts")))
    (consult-notes)
    (org-set-property "EXTRACT_OF"
		      (denote-retrieve-filename-identifier
		       (completing-read "Bibliography: "
					(denote-directory-files))))))

(defun denote-find-by-title (title &optional files-matching-regexp)
  (let (result)
    (catch 'found
      (dolist (file (denote-directory-files files-matching-regexp) result)
	(when (string= title (denote-retrieve-title-value file 'org))
	  (setq result file)
	  (throw 'found result))))))

(defcustom hydrang-english-month-names
  '("January" "February" "March" "April" "May" "June"
    "July" "August" "September" "October" "November" "December")
  "")

(defun hydrang-daily-tasks ()
  (interactive)
  (-let* (((year month day)
	   (mapcar #'string-to-number (split-string (org-read-date) "-")))
	  (title (format "%0#2d %s, %s" day month year))
          (file (denote-find-by-title title "__daily_organization_tasks"))
          (directory (format "%s/Planning/%s %s" denote-directory month year)))
    (unless (file-exists-p directory)
      (make-directory directory))
    (if file
        (find-file file)
      (denote title '("organization" "daily" "tasks") 'org directory))))

(defun hydrang-week-tasks ()
  (interactive)
  (let* ((date (decode-time))
	 (year (cl-sixth date))	 
	 (month (nth (1- (cl-fifth date)) hydrang-english-month-names))
	 (directory (format "%s/Planning/%s %s" denote-directory month year))
         (title (format-time-string "Week %V of %Y"))
         (file (denote-find-by-title title "__organization_tasks_weekly")))
    (unless (file-exists-p directory)
      (make-directory directory))
    (if file
        (find-file file)
      (denote title '("organization" "tasks" "weekly") 'org directory))))

(defun hydrang-month-tasks ()
  (interactive)
  (let* ((date (mapcar #'string-to-number (split-string (org-read-date) "-")))
	 (year (cl-first date))	 
	 (month (nth (1- (cl-second date)) hydrang-english-month-names))
         (title (format "%s of %s" month year))
         (file (denote-find-by-title title "__monthly_organization_tasks"))
         (directory (format "%s/Planning/%s %s" denote-directory month year)))
    (unless (file-exists-p directory)
      (make-directory directory))
    (if file
        (find-file file)
      (denote title '("organization" "tasks" "monthly") 'org directory))))

(defun hydrang-youtube-prompt ()
  (let ((minibuffer (read-from-minibuffer "Video: ")))
    (cond
     ((string-match-p "https://www\\.youtube\\.com/watch\\?v=.\\{11\\}" minibuffer)
      (replace-regexp-in-string "https://www\\.youtube\\.com/watch\\?v=\\(.\\{11\\}\\).*" "\\1" minibuffer))
     ((string-match-p "https://youtu\\.be/.\\{11\\}" minibuffer)
      (replace-regexp-in-string "https://youtu\\.be/\\(.\\{11\\}\\).*" "\\1" minibuffer))
     ((string-match-p "https://www\\.youtube\\.com/shorts/.\\{11\\}" minibuffer)
      (replace-regexp-in-string "https://www\\.youtube\\.com/shorts/\\(.\\{11\\}\\).*" "\\1" minibuffer))
     ((string-match-p "https://www\\.youtube\\.com/shorts/.\\{11\\}" minibuffer)
      (replace-regexp-in-string "https://www\\.youtube\\.com/shorts/\\(.\\{11\\}\\).*" "\\1" minibuffer))
     ((string-match-p ".\\{11\\}" minibuffer)
      minibuffer))))

(defun hydrang-youtube (id)
  (interactive (list (hydrang-youtube-prompt)))
  (let* ((video (ytel--API-call-synchronously (concat "videos/" id) '()))
	 (title (assoc-default 'title video))
	 (author (assoc-default 'author video))
	 (description (assoc-default 'description video))
	 (file (denote-find-by-title title "__videos")))
    (if file
	(find-file file)
      (denote title '("video") 'org (concat denote-directory "/Bibliography/Videos/YouTube"))
      (previous-line)
      (insert "#+author:     " author "\n#+url:        " (format "https://youtu.be/%s\n\n" id title))
      (insert (format "[[youtube:%s][%s]]\n\n" id title))
      (org-youtube-enable)
      (insert "* Description\n" description "\n* Notes\n* Cites")
      (org-cycle-global))))

(defcustom hydrang-resources-directory
  (concat denote-directory "/Resources/Videos/")
  "")

(defun hydrang-youtube-download (id)
  (interactive (list (hydrang-youtube-prompt)))
  (let* ((video-title (assoc-default 'title (ytel--API-call-synchronously (concat "videos/" id) `())))
	 (file-id (format-time-string denote-id-format))
	 (title (denote-sluggify-title video-title)))
    (ytdl--download-async (concat "https://youtu.be/" id)
			  (concat hydrang-resources-directory file-id "--" title "__video")
			  '("--merge-output-format" "mkv"))
    (hydrang-youtube id)
    (goto-char (point-min))
    (re-search-forward "\\[\\[youtube:")
    (next-line)
    (insert "\n[[file:"
	    (concat hydrang-resources-directory file-id "--" title "__video.mkv")
	    "][" video-title "]]\n")))

(defun hydrang-ytel ()
  (interactive)
  (let ((id (ytel-video-id (ytel-get-current-video))))
    (hydrang-youtube id)))

(defun hydrang-ytel-download ()
  (interactive)
  (let ((id (ytel-video-id (ytel-get-current-video))))
    (hydrang-youtube-download id)))

(defun hydrang-hand-diagram (title keywords)
  (interactive (list (denote-title-prompt)
                     (denote-keywords-prompt)))
  (let* ((id (format-time-string denote-id-format))
         (directory (concat denote-directory "/Hand/"))
         (file (format "%s%s--%s__%s.xopp"
                        directory
                        id
                        (denote-sluggify-title title)
                        (mapconcat #'identity (denote-keywords-sort keywords) "_"))))
    (copy-file "~/.dotfiles/templates/flow-map.xopp"
               file)
    (start-process-shell-command "xournal" nil
                                 (format "flatpak run com.github.xournalpp.xournalpp \"%s\"" file))))

(defun hydrang-hand-exercises (title keywords)
  (interactive (list (denote-title-prompt)
                     (denote-keywords-prompt)))
  (let* ((id (format-time-string denote-id-format))
         (directory (concat denote-directory "/Hand/"))
         (file (format "%s%s--%s__%s.xopp"
                        directory
                        id
                        (denote-sluggify-title title)
                        (mapconcat #'identity (denote-keywords-sort keywords) "_"))))
    (copy-file "~/.dotfiles/templates/practical.xopp"
               file)
    (start-process-shell-command "xournal" nil
                                 (format "flatpak run com.github.xournalpp.xournalpp \"%s\"" file))))

(push '("\\.xopp\\'" . "flatpak run com.github.xournalpp.xournalpp %s") org-file-apps)
(push '("\\.mp4\\'" . "mpv %s") org-file-apps)
(push '("\\.mkv\\'" . "mpv %s") org-file-apps)
(push '("\\.webm\\'" . "mpv %s") org-file-apps)

(use-package org-download
  :straight t
  :custom
  (org-download-image-dir (concat org-directory "Resources/"))
  (org-download-screenshot-method "sleep 2 && grim -g \"$(slurp)\" %s")
  (org-download-timestamp (concat denote-id-format "--"))
  (org-download-file-format-function (lambda (filename)
                                       (concat
                                        (format-time-string org-download-timestamp)
                                        (let* ((complete (split-string filename "\\." t " "))
                                               (extension (car (last complete)))
                                               (name (mapconcat #'identity (butlast complete))))
                                          (format "%s__image_resource.%s" name extension))))))

(use-package org-cliplink
  :straight t)

(defun hydrang-denote-school-task ()
  (car (denote-sort-files (denote-directory-files "__semester") 'identifier t)))

(defmacro hydrang-make-rename-denote-format (type)
  `(defun ,(intern (format "hydrang-rename-%s-denote" type)) ()
    (interactive)
    (let ((files (dired-get-marked-files)))
     (dolist (file files)
      (let ((title (denote-sluggify-title
		    (read-from-minibuffer "Title: " (file-name-base file))))
	    (directory (file-name-directory file))
	    (extension (file-name-extension file))
	    (id (format-time-string denote-id-format))
	    (filename (concat id "==" author "--" title
		       (format "__%s" type) "." extension)))
       (rename-file file
	(expand-file-name filename
	 dired)))))))

(hydrang-make-rename-denote-format article)
(hydrang-make-rename-denote-format book)

(provide 'hydrang-notes)
