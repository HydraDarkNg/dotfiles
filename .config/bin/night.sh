#!/bin/bash

night_mode() { 
    redshift -oP -O $1
    brightnessctl set $2
}

case $1 in 
  off) night_mode 6000 100%;; 
  *) night_mode 3500 50%;;
esac
