#!/bin/bash
SBCL="git://git.code.sf.net/p/sbcl/sbcl"
QUICKLISP="https://beta.quicklisp.org/quicklisp.lisp"
STUMPWM="https://github.com/stumpwm/stumpwm"
STUMPWM_MODULES="https://github.com/stumpwm/stumpwm-contrib.git"
CLX_TRUETYPE="https://github.com/lihebi/clx-truetype.git"

curl -O $QUICKLISP
echo "(load \"quicklisp.lisp\")
(quicklisp-quickstart:install)\
(ql:add-to-init-file)\
(ql:quickload \"clx\")\
(ql:quickload \"cl-ppcre\")\
(ql:quickload \"alexandria\")" > init.lisp
sbcl --load init.lisp
git clone $CLX_TRUETYPE
git clone $STUMPWM
cd stumpwm
./autogen.sh
./configure
make -j8
sudo make install
git clone $STUMPWM_MODULES
