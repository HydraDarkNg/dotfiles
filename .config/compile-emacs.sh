#!/bin/bash
FLAGS="--without-compress-install --with-native-compilation --with-json --with-mailutils --with-mailutils --with-tree-sitter --prefix=$(echo $HOME)/.local/usr"
REPO="git://git.sv.gnu.org/emacs.git"

rm -rf emacs
git clone -b master $REPO
cd emacs
./autogen.sh
./configure $FLAGS
make -j8
sudo make install
