(defpackage config
  (:use :cl :stumpwm))

(in-package config)

(ql:quickload :str)

(defun mapconcat (stream function list &optional (separator #\Space))
  (format stream
          (concatenate 'string "~{~a~^" (string separator) "~}")
          (mapcar function list)))

;; load-wallpaper
(run-shell-command "feh --bg-fill ~/Imágenes/Wallpapers/ --randomize")
;;load-cursor
(run-shell-command "xsetroot -cursor_name left_ptr")
;;(run-shell-command "emacs --daemon")
(run-shell-command "~/bin/Nextcloud-3.10.1-x86_64.AppImage")
;;load-picom
(defcommand picom () ()
  (run-shell-command "picom"))

(picom)

;; librewolf
(defcommand librewolf () ()
  (run-or-raise "~/bin/LibreWolf.x86_64.AppImage" '(:class "LibreWolf" :title "LibreWolf")))

(defcommand youtube (video) ((:string "Video: "))
 (run-or-raise
  (concat "~/bin/LibreWolf.AppImage \"http://localhost:3000/results?search_query="
          (cl-ppcre:regex-replace-all " " video "+") "\"")
  '(:class "LibreWolf" :title "LibreWolf")))

(defcommand web-search (busqueda) ((:string "Buscar en la web: "))
 (run-or-raise
  (concat "~/bin/LibreWolf.AppImage http://localhost:8000/?q="
          (cl-ppcre:regex-replace-all " " busqueda "+"))
  '(:class "LibreWolf" :title "LibreWolf")))

(defcommand web-url (busqueda) ((:string "URL: "))
 (run-or-raise
  (concat "~/bin/LibreWolf.AppImage " busqueda)
  '(:class "LibreWolf" :title "LibreWolf")))

(defcommand rae (palabra) ((:string "Ingresa la palabra: "))
 (run-or-raise (concat "~/bin/LibreWolf.AppImage https://dle.rae.es/" palabra "?m=form") '(:class "LibreWolf" :title "LibreWolf")))

(defcommand wikipedia (articulo) ((:string "Articulo: "))
   (run-or-raise
    (concat "~/bin/LibreWolf.AppImage https://es.wikipedia.org/wiki/"
            (cl-ppcre:regex-replace-all " " articulo "_")) '(:class "LibreWolf" :title "LibreWolf")))

(defcommand wikipedia-search (articulo) ((:string "Articulo: "))
 (run-or-raise
  (concat "~/bin/LibreWolf.AppImage \"https://es.wikipedia.org/w/index.php?search="
          articulo
          "%20&title=Especial%3ABuscar&fulltext=1&ns0=1&ns100=1&ns104=1\"")
  '(:class "LibreWolf" :title "LibreWolf")))

;; Firefox
(defcommand firefox () ()
  (run-or-raise "firefox" '(:class "firefox" :title "Firefox")))

;; ROFI
(defcommand rofi () ()
  (run-or-raise "rofi -show drun" '(:class "rofi")))

(defcommand rofi-run () ()
  (run-or-raise "rofi -show run" '(:class "rofi")))

 ;; NEXTCLOUD
(defcommand nextcloud () ()
  (run-or-raise "~/bin/Nextcloud-3.10.1-x86_64.AppImage" '(:class "Nextcloud")))

(load-module "globalwindows")

(set-prefix-key (kbd "s-x"))
(define-key *root-map* (kbd "c") "kitty")
(define-key *root-map* (kbd "e") "emacs")
(define-key *root-map* (kbd "C-m") "mode-line")
(define-key *root-map* (kbd "Q") "quit")
(define-key *root-map* (kbd "C-d") "rofi")
(define-key *root-map* (kbd "C-p") "rofi-run")
(define-key *root-map* (kbd "s-b") "windowlist")
(define-key *root-map* (stumpwm:kbd "w") "global-windowlist")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer set Master 5%+ unmute")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer set Master 5%- unmute")
(define-key *top-map* (kbd "XF86AudioMute") "exec amixer set Master toggle")
(define-key *top-map* (kbd "XF86AudioMicMute") "exec amixer set Capture toggle")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "exec brightnessctl set +5%")
(define-key *top-map* (kbd "XF86MonBrightnessDown") "exec brightnessctl set 5%-")
(define-key *top-map* (kbd "s-TAB") "fnext")
(define-key *top-map* (kbd "s-f") "fullscreen")
(define-key *top-map* (kbd "s-Right") "move-window right")
(define-key *top-map* (kbd "s-Left") "move-window left")
(define-key *top-map* (kbd "s-Up") "move-window up")
(define-key *top-map* (kbd "s-Down") "move-window down")
(define-key *top-map* (kbd "s-g") '*groups-map*)
(define-key *groups-map* (kbd "l") "grouplist")

(defparameter *applications* (make-sparse-keymap))

(define-key *top-map* (kbd "s-a") '*applications*)

(defparameter *firefox-functions*
  (let ((m (stumpwm:make-sparse-keymap)))
    (define-key m (stumpwm:kbd "f") "firefox")
    (define-key m (stumpwm:kbd "l") "librewolf")
    (define-key m (stumpwm:kbd "s") "web-search")
    (define-key m (stumpwm:kbd "r") "rae")
    (define-key m (stumpwm:kbd "y") "youtube")
    (define-key m (stumpwm:kbd "w") "wikipedia")
    (define-key m (stumpwm:kbd "u") "web-url")
    m))

(stumpwm:define-key *applications* (stumpwm:kbd "f") '*firefox-functions*)

;; Load slynk
(ql:quickload :slynk)

(let ((server-running nil)
      (first-time t))
  ;;
  (defcommand slynk () ()
    "Toggle the slynk server on/off."
    (if server-running
        (progn
          (slynk:stop-server 4005)
          (echo-string
           (current-screen)
           "Stopping slynk.")
          (setf server-running nil))
        (progn
          (slynk:create-server :port 4005
                               :style slynk:*communication-style*
                               :dont-close t)
          (if first-time
            (echo-string
             (current-screen)
             "Re-starting slynk.")
            (setf first-time nil))
          (setf server-running t)))))
;; Now call the re-defined command
(slynk)

(defparameter color1 "#fff")
(defparameter color2 "#000")
(defparameter color3 "#fff")
(defparameter color4 "#000")

(ql:quickload :clx)
(ql:quickload :clx-truetype)

(setf xft:*font-dirs* (cons "/home/HydraDarkNg/.local/share/fonts/TTF" xft:*font-dirs*))
;; (run-with-timer
;;  900 900
;;  (lambda ()
;;    (loop for font in (stumpwm::screen-fonts (current-screen))
;;          when (typep font 'xft:font)
;;            do (clrhash (xft::font-string-line-bboxes font))
;;               (clrhash (xft::font-string-line-alpha-maps font))
;;               (clrhash (xft::font-string-bboxes font))
;;         (clrhash (xft::font-string-alpha-maps font)))))

(load-module "ttf-fonts")
(xft:cache-fonts)

(set-font (list
           (make-instance 'xft:font
                          :family "Iosevka NF"
                          :subfamily "Bold"
                          :size 12
                          :antialias t)))

;; (defcommand emacs () ()
;;   (run-or-raise "emacsclient -c" '(:class "Emacs")))

(defcommand kitty () ()
  (run-or-raise "kitty --title=~" '(:class "kitty" :title "~")))

(defmacro emacsclient (&body body)
  `(string-trim
    '(#\" #\Newline)
    (run-shell-command
     (format nil "emacsclient -e '~(~a~)'" ',@body)
     t)))

(defmacro mpc (&rest args)
  `(run-shell-command
    (concatenate
     'string
     "mpc "
     ,@(mapcar (lambda (x)
                 (if (keywordp x)
                     (format nil "--~(~a~) " (symbol-name x))
                     (if (stringp x)
                         (format nil "\"~(~a~)\" " x)
                         (format nil "~(~a~) " x))))
               args))
    t))

(defun mpc-get-title-and-artist ()
  (let ((stream (make-string-input-stream (mpc :f "%Artist% - %Title%"))))  
    (read-line stream)))

(defun mpc-song-p ()
  (not (equal "volume:"
              (first (str:split #\Space (mpc-get-title-and-artist))))))

(defun mpc-song (ml)
  (declare (ignore ml))
  (if (mpc-song-p)
      (mpc-get-title-and-artist)
      ""))

(add-screen-mode-line-formatter #\s #'mpc-song)

(defun fmt-battery (ml)
  (declare (ignore ml))
  (second
   (str:split
    #\Space
    (str:trim
     (run-shell-command
      "upower -i $(upower -e | grep 'BAT')  | grep -E \"percentage\""
      t))
    :omit-nulls t)))

(add-screen-mode-line-formatter #\B 'fmt-battery)

(defun format-group-expand (format group)
  (if (group-windows group)
      (format-expand *group-formatters*
                     (concatenate 'string "^(:fg \"#006800\")" format "^n")
                     group)
      (format-expand *group-formatters*
                     format
                     group)))
(format-group-expand "[%t]" (current-group))

(defun format-of-group (group)
  (if (equal group (current-group))
      (format-group-expand "[%t]" group)
      (format-group-expand "%t" group)))

(defun fmt-groups (ml)
  (declare (ignore ml))
  (mapconcat nil #'format-of-group
             (stumpwm::sort-groups (current-screen))
             " "))
(fmt-groups t)

(add-screen-mode-line-formatter #\G 'fmt-groups)

(format-of-group (current-group))
(equal (current-group)
       (car (stumpwm::sort-groups (current-screen))))

(defun connection-name ()
  (run-shell-command
   "nmcli d show wlp0s20f3 | grep \"GENERAL.CONNECTION\""
   t))

(defun fmt-connection (ml)
  (declare (ignore ml))
  (second
   (str:split #\Space
              (str:trim (connection-name))
              :omit-nulls t)))

(add-screen-mode-line-formatter #\N 'fmt-connection)

(defun fmt-audio (ml)
  (declare (ignore ml))
  (run-shell-command "amixer get Master  | grep -Eo \"[0-9]+%\" | head -n1 | tr \"\\n\" \" \"" t))

(add-screen-mode-line-formatter #\A 'fmt-audio)

(defun opensuse-get-packages ()
  (with-open-file (file (merge-pathnames "~/.cache/packages"))
    (parse-integer (read-line file))))

(defun fmt-opensuse-packages (ml)
  (declare (ignore ml))
  (format nil "~d package~:*~[s~;~:;s~]" (opensuse-get-packages)))

(add-screen-mode-line-formatter #\P 'fmt-opensuse-packages)

(defun get-uptime-time ()
  (read-from-string
   (with-open-file (file #p"/proc/uptime")
     (read-line file))))

(defun fmt-uptime (ml)
  (declare (ignore ml))
  (let* ((days (floor (/ (get-uptime-time) 86400)))
         (rest-days (nth-value 1 (floor (/ (get-uptime-time) 86400))))
         (hours (floor (* rest-days 24)))
         (rest-hours (nth-value 1 (floor (* rest-days 24))))
         (minutes (floor (* rest-hours 60))))
    (format nil "~{~a~^, ~}"
            (remove-if
             #'null
             (list
              (when (< 0 days)
                (format nil "~dd" days))
              (when (< 0 hours)
                (format nil "~dh" hours))
              (when (< 0 minutes)
                (format nil "~dm"  minutes)))))))

(add-screen-mode-line-formatter #\U 'fmt-uptime)

(setf *group-format* "%t")
(setf *window-format* "%30t")

(defparameter *my-modeline*
  '("%G | %w ^>"
    (" %A %s" . "#d00000")
    ("󰁹 %B" . "#7c318f")
    " %P"
    ("󰥔  %d" . "#3548cf")
    "󰁡 %U"
    ("  %N" . "#00663f")))

(defun formating-modeline (list)
  (append
   (mapcar
    (lambda (x)
      (if (listp x)
          (cons (format nil "~a^n | " (car x))
                (cdr x))
          (format nil "~a^n | " x)))
    (butlast list))
   (last list)))

(defun generate-modeline (list)
  (mapcar
   (lambda (x)
     (if (listp x)
         (format nil "^(:fg \"~a\")~a^n" (cdr x) (car x))
         x))
   (formating-modeline list)))

(setf *screen-mode-line-format*
      (generate-modeline
       *my-modeline*)
      *mode-line-background-color* color1    
      *mode-line-foreground-color* color2
      *time-format-string-default* "%A %B %e %Y %k:%M:%S"
      *time-modeline-string* "%b %e %Y - (%k:%M)")

(toggle-mode-line (current-screen) (current-head))

(setf *message-window-gravity* :center
      *input-window-gravity* :center
      *window-border-style* :thin
      *message-window-padding* 10
      *maxsize-border-width* 5
      *normal-border-width* 0
      *transient-border-width* 2
      stumpwm::*float-window-border* 2
      stumpwm::*float-window-title-height* 5
      *message-window-y-padding* 10)

(set-bg-color color1)
(set-fg-color color2)
(set-border-color color2)
(set-msg-border-width 2)

(set-focus-color color2)
(grename "dev")
(gnewbg "www")
(gnewbg "media")
(gnewbg "video")

(define-frame-preference "www"
  (0 T T :title "Nextcloud")
  (0 T T :class "LibreWolf")
  (0 T T :class "firefox"))

(define-frame-preference "dev"
  (0 T T :class "Emacs")
  (0 T T :class "kitty"))

(define-frame-preference "media"
  (0 T T :class "Pcmanfm")
  (10 nil T :class "Pavucontrol"))

(define-frame-preference "video"
  (0 T T :class "mpv"))
